<!Doctype html>
<html lang="es">
	<head>
                <title><?= empty($title)?'PronexApp':$title ?></title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <?php 
                if(!empty($css_files) && !empty($js_files)):
                foreach($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
                <?php endforeach; ?>
                <?php foreach($js_files as $file): ?>
                <script src="<?= $file ?>"></script>
                <?php endforeach; ?>                
                <?php endif; ?>
                <? if(empty($crud) || empty($css_files)): ?>
                <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>                
                <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                <? endif ?>
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css') ?>">
                <script src="<?= base_url().'js/ace-extra.min.js' ?>"></script>	
                <script src="<?= base_url().'js/frame.js' ?>"></script>
                
                <script src="<?= base_url() ?>/js/jquery.Jcrop.min.js"></script>
                <script src="<?= base_url() ?>/js/jquery.color.js"></script>
                <link rel="stylesheet" href="<?= base_url() ?>/css/jquery.Jcrop.css" type="text/css" />                
        </head>
        <body class="no-skin">		
                <?php $this->load->view('includes/header') ?>
                <div class="main-container" id="main-container">
                        <?php $this->load->view('includes/sidebar') ?>
                        <div class="main-content">
                            <div class="main-content-inner">
                                <?php $this->load->view('includes/breadcum') ?>
                                <div class="page-content">						
                                    <?php $this->load->view($view) ?>                                            
                                </div><!-- /.page-content -->
                            </div>
                        </div><!-- /.main-content -->			
                </div><!-- /.main-container -->
                <script src="<?= base_url("js/ace.min.js") ?>"></script>
                <script src="<?= base_url("js/jquery-ui.custom.min.js") ?>"></script>	
                <script src="<?= base_url("js/ace-elements.min.js") ?>"></script>
        </body>
</html>
