angular.module('starter.controllers', [])

.controller('Menu', function($scope,$state,$http,$ionicPopup,$ionicLoading,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.goSearch = function(){
        $state.go('tab.Empresas');        
    };
    if(localStorage.user===undefined){
        document.location.href="index.html";
    }
    
    if(Api.lat==undefined){
        $scope.options = { enableHighAccuracy: true, maximumAge:60000, timeout:60000 };
        navigator.geolocation.getCurrentPosition(function(position) {
            $scope.position=position;
            var c = position.coords;
            $scope.$broadcast('getEmpresas',c.latitude, c.longitude);
            Api.lat = c.latitude;
            Api.lon = c.longitude;
         },function(e) { $scope.showAlert("No es posible acceder al GPS "+ e.code + " " + e.message) },$scope.options);
    }
    
    $scope.shareApp = function(){
        window.plugins.socialsharing.shareViaSMS('Hay una nueva aplicación para nuestras empresas, Pronex, si la descargas ahora es gratis. https://play.google.com/store/apps/details?id=com.pronexapp');
    };
    
    $scope.cuenta = function(){
        document.location.href="main.html#tab/cuenta";
    };
    
    $scope.$on('checkMyEmpresa',function(){        
        if(typeof(localStorage.misempresas)==='undefined'){
            Api.list('empresas',{usuarios_id:$scope.user},$scope,$http,function(data){
                localStorage.misempresas = data.length;
                if(localStorage.misempresas==='0'){
                    document.location.href="#/tab/miempresa";
                    $scope.showAlert('Para continuar usando nuestro servicio, debe crear una empresa');
                }
            });
        }else{
            if(localStorage.misempresas==='0'){
                document.location.href="#/tab/miempresa";
                $scope.showAlert('Para continuar usando nuestro servicio, debe crear una empresa');
            }
        }
    });
})

.controller('Main', function($scope,$http,$ionicLoading,Api,UI,User) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando empresas');    
    
    $scope.$on('MainGetEmpresas',function(lat,lon){
        $scope.getEmpresas(lat,lon);
    });
    
    $scope.getEmpresas = function(lat,lon){
        d = lat==undefined?{}:{lat:lat,lon:lon};
        Api.list('getFromDistance',d,$scope,$http,function(data){
            $scope.aprox = data[0];
            $scope.rank = data[1];
            $scope.contacts = data[2];
            $scope.$emit('checkMyEmpresa');
        });
    }
    if(Api.lat!=undefined){
        $scope.getEmpresas(Api.lat,Api.lon);
    }
    else{
        $scope.getEmpresas();
    }
    
    $scope.$on('getEmpresas',function(evt,lat,lon){
       $scope.getEmpresas(lat,lon); 
    });    
})

.controller('Cuenta', function($scope,$state,User) {
    $scope = User.getData($scope);
    
    $scope.desconectar = function(){
        User.cleanData();
        document.location.href="index.html";
    };
    
    $scope.actualizarDatos = function(){
        $state.go('tab.actualizarDatos');
    };
    
    $scope.Misempresas = function(){
        $state.go('tab.Misempresas');
    };
})

.controller('actualizarDatos', function($scope,$state,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
    $scope = User.getData($scope);
    $scope.action = 'actualizar';
    $scope.data = {nombre:$scope.nombre,email:$scope.email,password:$scope.password,telefono:$scope.telefono};
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    
    $scope.registrar = function(data){
        $scope.data = {nombre:data.nombre,email:data.email,password:data.password,telefono:data.telefono};        
        Api.update('usuarios',$scope.user,$scope,$http,function(data){
            $scope.showAlert('Sus datos han sido guardados');
            $scope.data.id = $scope.user;
            User.setData($scope.data);
        });
    };
    
})

.controller('Misempresas', function($scope,$state,$http,$ionicLoading,$ionicPopup,User,Api,UI) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Cargando empresas');
    //Traer empresas del usuario    
    Api.list('empresas',{usuarios_id:$scope.user},$scope,$http,function(data){
        $scope.empresas = data;
        localStorage.misempresas = data.length;
    });
    
    $scope.consultar = function(id){
        for(i in $scope.empresas){
            if(id==$scope.empresas[i].id){
                Api.selected = $scope.empresas[i];
                document.location.href='#/tab/miempresa/'+Api.selected.id;
            }
        }
    };
})

.controller('Miempresa', function($scope,$state,$stateParams,$state,$http,$ionicLoading,$ionicPopup,User,Api,UI,$ionicPopover) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.showConfirm = UI.getConfirmBox($ionicPopup);
    $scope.datos = {foto1:'',foto2:'',foto3:'',foto4:''};
    $scope.datos.ubicacion = {lat: 47.55633987116614,lon:7.576619513223015};
    $scope.addMapLink = true;
    //Traer sectores
    Api.list('sectores',{},$scope,$http,function(data){
        $scope.sectores = data;
    },'where');      
    
    $scope.changeEspecialidad = function(val){
        Api.list('especialidades',{sectores_id:val},$scope,$http,function(data){
            $scope.especialidades = data;
        },'where');
    };
    
    //Insertar    
    if($stateParams.id==undefined || $stateParams.id==''){
        $scope.datos.ubicacion = {lat:42.29633632697807,lon:3.3607765796874673};
        $scope.loading = UI.getLoadingBox($ionicLoading);
        $scope.registrar = function(data){
            $scope.data = data;
            $scope.data = $scope.getFiles(data);            
        };
    }
    //Editar
    else{
       $scope.loading = UI.getLoadingBox($ionicLoading);       
       Api.list('empresas',{id:$stateParams.id},$scope,$http,function(data){
            $scope.setDatos(data[0]);
       });
        
       $scope.registrar = function(data){
            $scope.data = data;
            $scope.data = $scope.getFiles(data);    
       }; 
    }
    
    $scope.setDatos = function(datos){
       $scope.datos = datos;
       $scope.datos.foto1preview = datos.foto1;
       $scope.datos.foto2preview = datos.foto2;
       $scope.datos.foto3preview = datos.foto3;
       $scope.datos.foto4preview = datos.foto4;       
       $scope.datos.ubicacion_geo = datos.ubicacion_geo;
       $scope.ubicacion_geo = datos.ubicacion_geo;
       $scope.datos.ubicacion = {lat: 47.55633987116614,lon:7.576619513223015};
       Api.list('especialidades',{sectores_id:datos.sectores_id},$scope,$http,function(data){
            $scope.especialidades = data;
       },'where');
    };
    
    $scope.getFiles = function(data){
        var action = $stateParams.id==undefined || $stateParams.id==''?'insert_validation':'update_validation/'+$stateParams.id;
        var action2 = $stateParams.id==undefined || $stateParams.id==''?'insert':'update/'+$stateParams.id;
        $scope.data = $scope.getData();
        $scope.loading('show');
        Api.remote(url+'empresas/'+action,'POST',$scope,$http,function(data){            
            data = data.replace('<textarea>','');
            data = data.replace('</textarea>','');
            data = JSON.parse(data);                          
            if(data.success){
                    $scope.loading('hide');
                    $scope.data = $scope.getData();
                    Api.remote(url+'empresas/'+action2,'POST',$scope,$http,function(data){
                    $scope.loading('hide');                        
                    $scope.showAlert('Sus datos han sido guardados');
                    $state.go('tab.main');
                    localStorage.misempresas = 1;
                    $scope.loading('hide');                                       
                });
            }
            else{
                $scope.showAlert(data.error_message);
                $scope.loading('hide');
            }            
        });
    };
    
    $scope.getData = function(){
        var d = document.getElementById('formreg');
        $scope.data = new FormData(d);
        if($scope.foto1===undefined){
            if($stateParams.id!=undefined && $stateParams.id!=''){
             $scope.data.append('foto1',$scope.datos.foto1);
            }
        }
        else{            
            $scope.data.append('foto1',$scope.datos.foto1preview);
        }
        if($scope.foto2!==null){
            $scope.data.append('foto2',$scope.datos.foto2preview);
        }
        if($scope.foto3!==null){
            $scope.data.append('foto3',$scope.datos.foto3preview);
        }
        if($scope.foto4!==null){
            $scope.data.append('foto4',$scope.datos.foto4preview);
        }
        return $scope.data;
    }
    
    
    
    $scope.delete = function(){
        $scope.showConfirm('Eliminar Empresa','¿Estás seguro que deseas eliminar esta empresa?',function(data){
            Api.deleterow('empresas',$scope.datos.id,$scope,$http,function(data){
                $scope.showAlert('Su empresa ha sido eliminada con exito');
                $state.go('tab.Misempresas');
            });
        });
    };
    
    $scope.location = function(data){
        $scope.datos.ubicacion_geo = '('+data.lat+','+data.lon+')';
        console.log(data);
    };
    
    $scope.triggerDevice = function(sourceType,foto){
        navigator.camera.getPicture(function(imageURI){
            $scope.selectImage(imageURI,foto);            
        },
        function(message) { $scope.showAlert('get picture failed'); },
        {
            quality: 50, 
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: sourceType,            
            targetWidth: 284, 
            targetHeight: 284,
            allowEdit: true
        }
        );
    };
    
    $scope.triggerFile = function(foto){        
        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
          template: 'Elige una opción para seleccionar la imagén',
          title: 'Dispositivo Multimedia',
          subTitle: 'Elige una opción',
          scope: $scope,
          buttons: [
            {
              text: '<b>Camara</b>',              
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.CAMERA,foto);
                  myPopup.close();
              }
            },
            {
              text: '<b>Galería</b>',
              onTap: function(e) {
                  $scope.triggerDevice(navigator.camera.PictureSourceType.SAVEDPHOTOALBUM,foto);
                  myPopup.close();
              }
            }
          ]
        });
        myPopup.then(function(res) {
          console.log('Tapped!', res);
        });
    };
    
    $scope.selectImage = function(imageURI,foto){
        switch(foto){
            case 'foto1':
                $scope.datos.foto1preview = "data:image/jpeg;base64,"+imageURI;
                $scope.foto1 = imageURI;
            break;
            case 'foto2':
                $scope.datos.foto2preview = "data:image/jpeg;base64,"+imageURI;
                $scope.foto2 = imageURI;
            break;
            case 'foto3':
                $scope.datos.foto3preview = "data:image/jpeg;base64,"+imageURI;
                $scope.foto3 = imageURI;                
            break;
            case 'foto4':
                $scope.datos.foto4preview = "data:image/jpeg;base64,"+imageURI;
                $scope.foto4 = imageURI;
            break;
        }
        if(!$scope.$$phase){
            $scope.$apply();
        }
    };    
})

.controller('Empresas', function($scope,$http,$ionicPlatform,$ionicLoading,$ionicModal,User,Api,UI) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando empresas');
    $scope.modal = UI.getModalBox($ionicModal,'templates/modals/advancedSearch.html',$scope);            
    
    Api.list('sectores',{},$scope,$http,function(data){
        $scope.sectores = data;
    });
    
    $scope.changeEspecialidad = function(val){
        Api.list('especialidades',{sectores_id:val},$scope,$http,function(data){
            $scope.especialidades = data;
        },'where');
    };
              
    $scope.advancedSearch = function(){
        $scope.modal = UI.getModalBox($ionicModal,'templates/modals/advancedSearch.html',$scope,function(){
                $scope.toggleModal('show');
        });
    };
    
    $scope.closeModal = function(){
        $scope.toggleModal('hide');
        //$scope.modal = UI.getModalBox($ionicModal,'templates/modals/advancedSearch.html',$scope);
    };
    
    $scope.search = function(datos){
        if(datos!==undefined){
            if(typeof(datos.zip)==='undefined' || datos.zip==='' || (datos.zip.toString().length===5)){
                Api.list('searchEmpresas',datos,$scope,$http,function(data){
                    $scope.empresas = data;
                    $scope.closeModal();
                },'where');
            }else{
                $scope.showAlert('El codigo cp que ha introducido no es correcto, verifiquelo e intente nuevamente');
            }
        }else{
            $scope.showAlert('Debe seleccionar algún críterio de búsqueda');
        }
    };
    $ionicPlatform.ready(function(){
        $scope.advancedSearch();
    });
    $scope.names = ["hola"];
})

.controller('EmpresaDetail', function($scope,$stateParams,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,$ionicSlideBoxDelegate, User,Api,UI) {
    $scope = User.getData($scope);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Buscando empresa');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.empresa = {};
    $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
    $scope.refreshLocation = function(){
        
    };
    $ionicSlideBoxDelegate.slidesCount(2);
    
    $scope.getEmpresa = function(){
        var lat = Api.lat===undefined?40.41373548106751:Api.lat;
        var lon = Api.lon===undefined?-3.706865632226595:Api.lon;
        $scope.data = {'search_field[]':'id','search_text[]':$stateParams.id,usuarios_id:$scope.user,lat:lat,lon:lon};
        Api.query('empresas/json_list',$scope,$http,function(data){
            $scope.empresa = data[0];        
            $scope.basel = {lat:data[0].lat,lon:data[0].lon};
            if(!$scope.$$phase){            
                $scope.$apply('basel');
                $scope.$apply('empresa');
                $scope.$apply();
            }
        });
    };
    
    $scope.datos = {calidad:0,limpieza:0,rapidez:0,confianza:0,seriedad:0,usuarios_id:$scope.user};
    $scope.rankear = function(datos){
        $scope.data = datos;
        $scope.data.empresas_id = $scope.empresa.id;
        $scope.loading = UI.getLoadingBox($ionicLoading,'Guardando ranking');
        Api.insert('ranking',$scope,$http,function(data){
           $scope.showAlert('Ranking','Su ranking se ha realizado satisfactoriamente');
        });
    };
    
    $scope.share = function(){        
        window.plugins.socialsharing.share($scope.empresa.nombre+' '+$scope.empresa.s986e880e+' Telefono: '+$scope.empresa.telefono+' Website: '+$scope.empresa.website);
    };
    
    $scope.openURL = function(url){
        console.log(url);
        window.open(url, '_blank', 'location=yes');
    };
    
    $scope.comollegar = function(){
        
        var addressLongLat = $scope.empresa.ubicacion_geo;
        addressLongLat = addressLongLat.replace('(','');
        addressLongLat = addressLongLat.replace(')','');
        window.open("geo:"+addressLongLat+'?q='+addressLongLat+' ('+$scope.empresa.nombre+')','_system');
    };
    
    $scope.getEmpresa();
})

.controller('Gestor', function($scope,$stateParams,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,$ionicSlideBoxDelegate, User,Api,UI) {
    $scope = User.getData($scope);
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.$emit('checkMyEmpresa');
    $scope.search = function(params){
        $scope.$emit('checkMyEmpresa');
        if(params.fecha!==undefined && params.fecha!==''){
            Api.fecha = params.fecha;
            Api.direccion = params.direccion;
            $state.go('tab.gestorTierra');
        }
        else{
            $scope.showAlert('Gestor de tierras','Debe introducir una fecha para consultar');
        }
    };
    
    $scope.registrar = function(params){
        $scope.$emit('checkMyEmpresa');
        if(params.fecha!==undefined && params.fecha!==''){
            Api.fecha = params.fecha;
            Api.direccion = params.direccion;
            $state.go('tab.addTierra');
        }
        else{
            $scope.showAlert('Gestor de tierras','Debe introducir una fecha para consultar');
        }
    }
})

.controller('GestorTierra', function($scope,$stateParams,$ionicPlatform,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,$ionicSlideBoxDelegate, User,Api,UI) {
    $scope = User.getData($scope);
    $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
    $scope.marks = [];
    $scope.direccion = Api.direccion!=undefined?Api.direccion:null;
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Consultando Puntos');
    $scope.modal = UI.getModalBox($ionicModal,'templates/modals/datoscliente.html',$scope);
    
    $scope.updatePosition = function(){
        if($scope.direccion==null){
            $scope.options = { enableHighAccuracy: true, maximumAge:60000, timeout:60000 };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);
                $scope.$apply();                
                $scope.showAlert('Confirma tu ubicación','Si sientes que el GPS no ha encontrado tu ubicación, puedes arrastrar el marcador hacia tu ubicación exacta');
             },function(e) { $scope.showAlert("No es posible acceder al GPS "+ e.code + " " + e.message) },$scope.options);

            $scope.gotoLocation = function (lat, lon) {
                if ($scope.lat != lat || $scope.lon != lon) {
                         $scope.basel = { lat: lat, lon: lon };
                         if (!$scope.$$phase) $scope.$apply("basel");
                     }
                }
        }
        else{
            $scope.showAlert('Confirma tu ubicación','Pulsa sobre alguno de los iconos del mapa para saber cual es tu ubicación');
        }
    }
    $scope.location = function(value){
        $scope.basel = value;
        if (!$scope.$$phase) $scope.$apply("basel");
        $scope.$apply();
        
        $scope.data = {lat:$scope.basel.lat,lon:$scope.basel.lon,fecha:Api.fecha};
        Api.query('viewLands',$scope,$http,function(data){
            $scope.marks = data;
            if (!$scope.$$phase){
                $scope.$apply("marks"); 
                $scope.$apply();
            }                    
        });
    }
    
    $scope.markersclick = function(datos){
        console.log(datos);
        $scope.datosCliente = datos;
        if(!$scope.$$phase){
            $scope.$apply();
        }
        $scope.toggleModal('show');        
    }
    
    $scope.$watch('basel',function(){
       console.log($scope.basel); 
    });
    
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    
    $scope.confirmar = function(){
        console.log('Confirmado');
    };  
    
    $scope.closeModal = function(){
        $scope.toggleModal('hide');
        $scope.modal = UI.getModalBox($ionicModal,'templates/modals/datoscliente.html',$scope);
    }
})

.controller('addTierra', function($scope,$stateParams,$ionicPlatform,$state,$http,$ionicLoading,$ionicPopup,$ionicModal,$ionicSlideBoxDelegate, User,Api,UI) {
    $scope = User.getData($scope);
    $scope.basel = { lat: 47.55633987116614, lon: 7.576619513223015 };
    $scope.direccion = Api.direccion!=undefined?Api.direccion:null;
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.loading = UI.getLoadingBox($ionicLoading,'Almacenando Punto');
    
    $scope.updatePosition = function(){
        if($scope.direccion==null){
            $scope.options = { enableHighAccuracy: true, maximumAge:60000, timeout:60000 };
            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.position=position;
                var c = position.coords;
                $scope.gotoLocation(c.latitude, c.longitude);
                $scope.$apply();                
                $scope.showAlert('Confirma tu ubicación','Si sientes que el GPS no ha encontrado tu ubicación, puedes arrastrar el marcador hacia tu ubicación exacta');
             },function(e) { $scope.showAlert("No es posible acceder al GPS "+ e.code + " " + e.message) },$scope.options);

            $scope.gotoLocation = function (lat, lon) {
                if ($scope.lat != lat || $scope.lon != lon) {
                         $scope.basel = { lat: lat, lon: lon };
                         if (!$scope.$$phase) $scope.$apply("basel");
                     }
                }
        }
        else{
            $scope.showAlert('Confirma tu ubicación','Pulsa sobre alguno de los iconos del mapa para saber cual es tu ubicación');
        }
    }
    $scope.location = function(value){
        $scope.basel = value;
        if (!$scope.$$phase) $scope.$apply("basel");
        $scope.$apply();
        
        $scope.data = {lat:$scope.basel.lat,lon:$scope.basel.lon,fecha:Api.fecha,usuarios_id:$scope.user};
        Api.insert('gestor',$scope,$http,function(data){
            $scope.showAlert('Añadir gestor','Añadido el punto al gestor de tierras');
        });
    }
    
    $scope.$watch('basel',function(){
       console.log($scope.basel); 
    });
    
    $ionicPlatform.ready(function() {
            $scope.updatePosition();             
    });
    
    $scope.confirmar = function(){
        console.log('Confirmado');
    };        
});
