MapApp.directive("appMap", function ($window) {
    return {
        restrict: "AE",
        replace: true,        
        template: "<div data-tap-disabled='true'></div>",
        scope: {
            center: "=",        // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
            markers: "=",       // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
            map:'=',
            direccion:'=',
            location:'=',
            markersclick:'=',
            widthmap: "@",         // Map width in pixels.
            heightmap: "@",        // Map height in pixels.
            zoom: "@",          // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
            mapTypeId: "@",     // Type of tile to show on the map (roadmap, satellite, hybrid, terrain).
            panControl: "@",    // Whether to show a pan control on the map.
            zoomControl: "@",   // Whether to show a zoom control on the map.
            scaleControl: "@"   // Whether to show scale control on the map.
        },
        link: function (scope, element, attrs,ngModelCtrl) {
            var toResize, toCenter;
            var map;
            var infowindow;
            var currentMarkers;
            var callbackName = 'InitMapCb';
            var positionMark = null;
            var el = element[0];
            var lugares = [];
            element[0].style.width = scope.widthmap===undefined?window.innerWidth+'px':scope.widthmap;
            element[0].style.height = scope.heightmap===undefined?window.innerHeight+'px':scope.heightmap;
            console.log(element[0]);
            console.log(scope);
            // callback when google maps is loaded
            $window[callbackName] = function() {
                createMap();
                updateMarkers();
            };

            if (!$window.google || !$window.google.maps ) {
                console.log("map: not available - load now gmap js");
                loadGMaps();
            }
            else{
                console.log("map: IS available - create only map now");
                createMap();
            }
            
            function loadGMaps() {
                console.log("map: start loading js gmaps");
                var script = $window.document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=true&callback=InitMapCb';
                $window.document.body.appendChild(script);
            }

            function createMap() {
                    console.log("map: create map start");
                    var mapOptions = {
                                zoom: parseInt(scope.zoom),
                                center: new google.maps.LatLng(scope.center.lat,scope.center.lon),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                panControl: true,
                                zoomControl: true,
                                mapTypeControl: true,
                                scaleControl: false,
                                streetViewControl: false,
                                navigationControl: true,
                                disableDefaultUI: true,
                                overviewMapControl: true
                    };
                    
                    if (!(map instanceof google.maps.Map)) {
                            console.log("map: create map now as not already available ");
                            map = new google.maps.Map(element[0], mapOptions);
                            positionMark = new google.maps.Marker({ draggable:true, position: new google.maps.LatLng(scope.center.lat,scope.center.lon), map: map, title: 'Mi posición' });                            
                            if(scope.direccion!=null){
                                searchDireccion(scope.direccion);                                
                            }
                            
                            google.maps.event.addDomListener(positionMark, 'mouseup', function(e) {
                                    if(confirm('¿Seguro que esta es tu ubicación?')){
                                        scope.location({lat:e.latLng.lat(),lon:e.latLng.lng()});
                                    }
                            });                                                       
                     }
             }

            scope.$watch('center', function() {                    
                    updateMarkers();                    
            });
            
            scope.$watch('markers', function() {                    
                    drawMarks();                    
            });
            
            function searchDireccion(direccion){
                if (map) {
                    var request = {
                        location: new google.maps.LatLng(scope.center.lat,scope.center.lon),
                        radius: '500',
                        query: direccion
                      };
                    service = new google.maps.places.PlacesService(map);
                    service.textSearch(request,function(results, status){
                            if (status == google.maps.places.PlacesServiceStatus.OK) {                                 
                                  for(i in results){
                                      CrearMarcas(results[i]);
                                  }
                                  l = results[0].geometry.location;
                                  scope.center = {lat:l.lat(),lon:l.lng()};
                                  positionMark.setMap(null);
                                  updateMarkers();
                            }
                    })
                }
            }
            
            function CrearMarcas(place){
                loc = place.geometry.location;
                m = new google.maps.Marker({ position: new google.maps.LatLng(loc.lat(),loc.lng()), map: map, title: place.name, icon:place.icon });
                google.maps.event.addDomListener(m, 'mouseup', function(e) {
                        scope.location({lat:e.latLng.lat(),lon:e.latLng.lng()});
                });
                lugares.push(m);
            }
            
            function drawMarks(){                
                for(i in scope.markers){
                    l = scope.markers[i];
                    m = new google.maps.Marker({ position: new google.maps.LatLng(l.lat,l.lon), map: map, title: l.nombre,icon:'http://www.pronexapp.com/newapp/images/webapp/pala.png'});
                    m.data = l;
                    m.drawEvent = function(){
                        google.maps.event.addDomListener(m, 'mouseup', function(e) {
                                    scope.markersclick(this.data)
                         });
                   }
                   m.drawEvent();
                }
                
                for(i in lugares){
                    lugares[i].setMap(null);
                }
            }
            
            function updateMarkers() {                    
                    if (map) {                            
                        map.panTo(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                        positionMark.setPosition(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                    }
            }

            // convert current location to Google maps location
            function getLocation(loc) {
                    if (loc == null) return new google.maps.LatLng(40, -73);
                    if (angular.isString(loc)) loc = scope.$eval(loc);
                    return new google.maps.LatLng(loc.lat, loc.lon);
                    }

            } // end of link:
    }; // end of return
});

/*MapApp.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    
                }
            });
        };
    });*/