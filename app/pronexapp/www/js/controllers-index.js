angular.module('starter.controllers', [])

.controller('Login', function($scope,$state,$http,$ionicLoading,$ionicModal,$ionicPopup,Api,UI,User){
    
       $scope.action = 'insertar';

       $scope.loading = function(attr){            
            attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: 'Cargando...'});
       };
       
       $scope.showAlert = UI.getShowAlert($ionicPopup);
        
       $scope.validEnter = function(){
            if(localStorage.email!==undefined){                     
               $scope = User.getData($scope);
               document.location.href="main.html";
            }
       }

       $scope.login = function(param){
            $scope.email = param.email;
            $scope.password = param.password;
            $scope.isRegister();
       }

       $scope.isRegister = function(){
           data =  {email:$scope.email,password:$scope.password};           
           Api.list('usuarios',data,$scope,$http,function(data){
                 if(data.length==0){
                    buttonAccept = {
                        text: 'Registrar empresa',
                        type: 'button-balanced',
                        onTap: function(e) {
                            $state.go('tab.registrar');
                            User.email = $scope.email;
                        }
                    }
                    UI.showPopupBox($ionicPopup,$scope,'Email o Contraseña Incorrecta','Inicio de sesión',buttonAccept);
                 }
                 else{
                       data = data[0];
                       User.setData(data);
                       document.location.href="main.html";
                 }
           });         
       }
       $scope.validEnter();
       
       /*Registrar*/
       if(User.email!=undefined){
           $scope.data = {email:User.email}
       }
       $scope.registrar = function(data){
           if(data != undefined){
            if(data.politicas !=undefined){
                 $scope.data = {nombre:data.nombre, email:data.email, password:data.password, telefono:data.telefono}
                 $scope.email = data.email;
                 $scope.password = data.password;
                 Api.insert('usuarios',$scope,$http,function(data){                     
                     $scope.isRegister();
                 });
             }
             else{
                 $scope.showAlert('Debe aceptar las condiciones');
             }
           }           
       };
       
       $ionicModal.fromTemplateUrl('templates/condiciones.html', {//Invitar personas
            scope: $scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modal = modal;
          });
                    
        $scope.openModal = function() {
          $scope.modal.show();
        };
        $scope.closeModal = function() {
          $scope.modal.remove();
        };
        
        $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {                
            $scope.closeModal();
        }); 
          
       
       $scope.mostrarCondiciones = function(){
         $scope.data = {id:null}
         Api.query('condiciones',$scope,$http,function(data){
            $scope.condiciones = data;
            $scope.openModal();
         });
       };
})

.controller('Recuperar', function($scope,$state,$http,$ionicLoading,$ionicModal,$ionicPopup,Api,UI,User){
    $scope.loading = UI.getLoadingBox($ionicLoading,'Enviando datos');
    $scope.showAlert = UI.getShowAlert($ionicPopup);
    $scope.recuperar = function(datos){
        $scope.data = datos;
        Api.query('recuperarPass',$scope,$http,function(data){
            $scope.showAlert(data);
        });
    }       
});
