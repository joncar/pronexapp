MapApp.directive('placesAutocomplete',function($window){
    return {
        restrict:"AE",
        replace:true,
        template:'<input type="text">',
        scope:{
            selectfields:'='
        },
        link:function(scope,element,attrs){
            var callbackName = 'InitMapCb';
            var input = element[0];
            var autocomplete;
            $window[callbackName] = function() {                
                autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
                autocomplete.addListener('place_changed', fillInAddress);
            };
            
            
             if (!$window.google || !$window.google.maps ) {
                console.log("map: not available - load now gmap js");
                loadGMaps();
            }else{
                autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
                autocomplete.addListener('place_changed', fillInAddress);
            }
            function loadGMaps() {
                console.log("map: start loading js gmaps");
                var script = $window.document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry&sensor=true&callback=InitMapCb';
                $window.document.body.appendChild(script);
            }
            
            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();               
                scope.selectfields(place);
              }
        }
    }
});
MapApp.directive("appMapReg", function ($window) {
    return {
        restrict: "E",
        replace: true,        
        template: "<div data-tap-disabled='true'></div>",
        scope: {
            center: "=",        // Center point on the map (e.g. <code>{ latitude: 10, longitude: 10 }</code>).
            markers: "=",       // Array of map markers (e.g. <code>[{ lat: 10, lon: 10, name: "hello" }]</code>).
            map:'=',
            direccion:'=',
            location:'=',
            icon:'=',
            width: "=",         // Map width in pixels.
            height: "=",        // Map height in pixels.
            zoom: "@",          // Zoom level (one is totally zoomed out, 25 is very much zoomed in).
            mapTypeId: "@",     // Type of tile to show on the map (roadmap, satellite, hybrid, terrain).
            panControl: "@",    // Whether to show a pan control on the map.
            zoomControl: "@",   // Whether to show a zoom control on the map.
            scaleControl: "@"   // Whether to show scale control on the map.
        },
        link: function (scope, element, attrs,ngModelCtrl) {
            var toResize, toCenter;
            var map;
            var infowindow;
            var currentMarkers;
            var callbackName = 'InitMapCb';
            var positionMark = null;
            var el = element[0];            
            el.style.width = '100%';            
            el.style.height = '200px';
            console.log(scope);
            // callback when google maps is loaded
            $window[callbackName] = function() {
                createMap();
                updateMarkers();
            };

            if (!$window.google || !$window.google.maps ) {
                console.log("map: not available - load now gmap js");
                loadGMaps();
            }
            else{
                console.log("map: IS available - create only map now");
                createMap();
            }
            
            function loadGMaps() {
                console.log("map: start loading js gmaps");
                var script = $window.document.createElement('script');
                script.type = 'text/javascript';
                script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry&callback=InitMapCb';
                $window.document.body.appendChild(script);
            }

            function createMap() {
                    console.log("map: create map start");
                    var mapOptions = {
                                zoom: parseInt(scope.zoom),
                                center: new google.maps.LatLng(scope.center.lat,scope.center.lon),
                                mapTypeId: google.maps.MapTypeId.ROADMAP,
                                panControl: true,
                                zoomControl: true,
                                mapTypeControl: true,
                                scaleControl: false,
                                streetViewControl: false,
                                navigationControl: true,
                                disableDefaultUI: true,
                                overviewMapControl: true
                    };
                    
                    if (!(map instanceof google.maps.Map)) {
                            console.log("map: create map now as not already available ");
                            map = new google.maps.Map(element[0], mapOptions);
                            console.log(element[0]);
                            positionMark = new google.maps.Marker({ draggable:true, position: new google.maps.LatLng(scope.center.lat,scope.center.lon), map: map, title: 'Mi posición',icon:scope.icon });                            
                            console.log(scope.direccion);
                            if(scope.direccion!=null && scope.direccion!=undefined && scope.direccion!=' '){                                
                                searchDireccion(scope.direccion);
                            }
                            //google.maps.event.addDomListener(positionMark, 'mouseup', function(e) {
                            google.maps.event.addDomListener(positionMark, 'dragend', function(e) {
                                    if(confirm('¿Seguro que esta es tu ubicación?')){
                                        scope.location({lat:e.latLng.lat(),lon:e.latLng.lng()});
                                    }
                            });
                     }
             }

            scope.$watch('center', function() {                    
                    updateMarkers();
            });
            
            scope.$watch('direccion',function(){
               searchDireccion(scope.direccion); 
            });
            
            function searchDireccion(direccion){
                if (map) {
                    var geocoder = new google.maps.Geocoder();
                    var address = direccion;
                    geocoder.geocode({'address': address}, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            for(var i in results){                                    
                                CrearMarcas(results[i]);
                            }
                            if(results.length===1){
                                var loc = results[0].geometry.location;
                                scope.location({lat:loc.lat(),lon:loc.lng()});
                            }
                        } else {
                          console.log('No se ha podido encontrar la ubicación indicada: ' + status);
                          //window.history.back();
                        }
                    });
                }
            }
            
            function CrearMarcas(place){
                var loc = place.geometry.location;
                m = new google.maps.Marker({ position: new google.maps.LatLng(loc.lat(),loc.lng()), map: map, title: place.name,icon:place.icon});
                google.maps.event.addDomListener(m, 'mouseup', function(e) {
                        scope.location({lat:e.latLng.lat(),lon:e.latLng.lng()});
                });   
            }
            
            function updateMarkers() {                    
                    if (map) {                            
                        map.panTo(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                        positionMark.setPosition(new google.maps.LatLng(scope.center.lat,scope.center.lon));
                    }
            }

            // convert current location to Google maps location
            function getLocation(loc) {
                    if (loc == null) return new google.maps.LatLng(40, -73);
                    if (angular.isString(loc)) loc = scope.$eval(loc);
                    return new google.maps.LatLng(loc.lat, loc.lon);
                    }

            } // end of link:
    }; // end of return
});