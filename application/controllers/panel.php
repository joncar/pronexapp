<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                if(empty($_SESSION['user'])){
                    header("Location:".base_url());
                    exit;
                }
        }
        
        public function loadView($data)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('main?redirect='.$_SERVER['REQUEST_URI']));
            if(!$this->user->hasAccess()){
                throw  new Exception('<b>ERROR: 403<b/> Usted no posee permisos para realizar esta operación','403');
            }
            else{
                if(!empty($data->output)){
                    $data->view = empty($data->view)?'panel':$data->view;
                    $data->crud = empty($data->crud)?'user':$data->crud;
                    $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
                }
                parent::loadView($data);            
            }
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            if($this->user->admin==0){
                $crud->set_model('usuario_model');
            }
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        function seleccionarFacultad($x = '',$nombre = ''){
            if(!empty($x) && is_numeric($x)){
                $this->user->setVariable('facultad',$x);
                $this->user->setVariable('facultadName',  urldecode($nombre));
                redirect('panel');
            }
            else{
                $crud = new ajax_grocery_CRUD();
                $crud->set_table('facultades');
                $crud->set_theme('bootstrap2');
                $crud->set_subject('Seleccione una sucursal');
                foreach($this->user->getAccess('facultades.*')->result() as $p)
                    $crud->or_where('id',$p->id);
                $crud->unset_add()
                        ->unset_edit()
                        ->unset_delete()
                        ->unset_print()
                        ->unset_export()
                        ->unset_read();
                $crud->set_field_upload('foto','img/fotos_facultades');
                $crud->columns('foto','nombre');
                $crud->callback_column('nombre',function($val,$row){
                    return '<a href="'.base_url('panel/seleccionarFacultad/'.$row->id.'/'.$row->nombre).'">'.$val.'</a>';
                });
                $crud = $crud->render();
                $this->loadView($crud);
            }
        }               
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
