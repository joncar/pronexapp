<?php
require_once APPPATH.'/controllers/main.php'; 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Main {
        
        public function __construct()
        {
                parent::__construct();
                $this->load->library('grocery_crud');
                $this->load->library('ajax_grocery_crud');
                $this->load->model('seguridadModel');
                $this->load->model('querys');
        }
        
        public function loadView($data)
        {
             if(!empty($data->output)){
                $data->view = empty($data->view)?'panel':$data->view;
                $data->crud = empty($data->crud)?'user':$data->crud;
                $data->title = empty($data->title)?ucfirst($this->router->fetch_method()):$data->title;
            }
            parent::loadView($data);
        }
        
        protected function crud_function($x,$y,$controller = ''){
            $crud = new ajax_grocery_CRUD($controller);
            $crud->set_theme('bootstrap2');
            $table = !empty($this->as[$this->router->fetch_method()])?$this->as[$this->router->fetch_method()]:$this->router->fetch_method();
            $crud->set_table($table);
            $crud->set_subject(ucfirst($this->router->fetch_method()));            
            $crud->required_fields_array();
            
            if(method_exists('panel',$this->router->fetch_method()))
             $crud = call_user_func(array('panel',$this->router->fetch_method()),$crud,$x,$y);
            return $crud;
        }
        
        public function usuarios($x = '',$y = ''){
            $crud = $this->crud_function('',''); 
            $crud->unset_delete();
            $crud->field_type('fecha_registro','invisible');
            if(empty($y) || !empty($_POST) && $_POST['email']!=$this->db->get_where('usuarios',array('id'=>$y))->row()->email){
                $crud->set_rules('email','Email','required|valid_email|is_unique[usuarios.email]');
            }
            $crud->callback_before_insert(function($post,$id){
                $post['fecha_registro'] = date("Y-m-d H:i:s");
                return $post;
            });
            $crud->callback_after_insert(function($post,$id){
                $this->sendMail($this->db->get_where('usuarios',array('usuarios.id'=>$id))->row(),1);
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function empresas($x = '',$y = ''){
            if(empty($_POST['operator'])){
                $_POST['operator'] = 'where';
            }
            
            if(!empty($_POST['search_field']) && in_array('id',$_POST['search_field'])){
                foreach($_POST['search_field'] as $n=>$v){
                    if($v=='id'){
                        $id = $_POST['search_text'][$n];
                    }
                }
                
                if(!empty($id)){
                    if($this->db->get_where('visitas',array('empresas_id'=>$id,'usuarios_id'=>$_POST['usuarios_id']))->num_rows==0){
                        $this->db->insert('visitas',array('empresas_id'=>$id,'usuarios_id'=>$_POST['usuarios_id']));
                        $contacts = $this->db->get_where('visitas',array('empresas_id'=>$id))->num_rows;
                        $this->db->update('empresas',array('contacts'=>$contacts),array('id'=>$id));
                    }
                }
            }
            
            $crud = $this->crud_function('','');
            $crud->callback_column('foto1',function($val){return base_url('images/fotos_empresas/'.$val);});
            $crud->callback_column('foto2',function($val){return strstr($val,'.')?base_url('images/fotos_empresas/'.$val):'null';});
            $crud->callback_column('foto3',function($val){return strstr($val,'.')?base_url('images/fotos_empresas/'.$val):'null';});
            $crud->callback_column('foto4',function($val){return strstr($val,'.')?base_url('images/fotos_empresas/'.$val):'null';});
            $crud->callback_column('facebook',array($this,'link'));
            $crud->callback_column('twitter',array($this,'link'));
            $crud->callback_column('linkdedin',array($this,'link'));
            $crud->callback_column('website',array($this,'link'));
            $crud->callback_column('descripcion',function($val,$row){return $row->descripcion;});
            $crud->callback_column('nombre',function($val,$row){return $row->nombre;});
            $crud->callback_column('distance',function($val,$row){
                if(!empty($_POST['lat'])){
                $lat = $row->lat;
                $lon = $row->lon;
                $distancia = 0;
                $distancia = round(( 3959*acos(cos(deg2rad($_POST['lat']))*cos(deg2rad($lat))*cos(deg2rad($lon)-deg2rad($_POST['lon']))+sin(deg2rad($_POST['lat']))*sin(deg2rad($lat))))/0.62137,1);
                //die();
                return $distancia;
                }
                else{
                    return '0';
                }
            });
            $crud->callback_column('especialidades',function($val,$row){
                $e = array();
                foreach(get_instance()->db->get_where('empresas_especialidades',array('empresas_id'=>$row->id))->result() as $es){
                    $e[] = $es->especialidades_id;
                }
                return json_encode($e);
            });
            $crud->callback_before_insert(function($post){
                if(!empty($post['foto1']) && strstr($post['foto1'],'data:image/jpeg;base64,')){
                    $post['foto1'] = $this->uploadPhoto($post['foto1']);
                }
                if(!empty($post['foto2']) && strstr($post['foto2'],'data:image/jpeg;base64,')){
                    $post['foto2'] = $this->uploadPhoto($post['foto2']);
                }
                if(!empty($post['foto3']) && strstr($post['foto3'],'data:image/jpeg;base64,')){
                    $post['foto3'] = $this->uploadPhoto($post['foto3']);
                }
                if(!empty($post['foto4']) && strstr($post['foto4'],'data:image/jpeg;base64,')){
                    $post['foto4'] = $this->uploadPhoto($post['foto4']);
                }
                $p = $post['ubicacion_geo'];
                $p = str_replace('(','',$p);
                $p = str_replace(')','',$p);
                $p = explode(',',$p);
                $post['lat'] = $p[0];
                $post['lon'] = $p[1];
                return $post;
            });
            
            $crud->callback_before_update(function($post,$id){
                if(!empty($post['foto1']) && strstr($post['foto1'],'data:image/jpeg;base64,')>-1){
                    $post['foto1'] = $this->uploadPhoto($post['foto1']);
                }else{
                    $post['foto1'] = $this->db->get_where('empresas',array('id'=>$id))->row()->foto1;
                }
                if(!empty($post['foto2']) && strstr($post['foto2'],'data:image/jpeg;base64,')>-1){
                    $post['foto2'] = $this->uploadPhoto($post['foto2']);
                }else{
                    $post['foto2'] = $this->db->get_where('empresas',array('id'=>$id))->row()->foto2;
                }
                if(!empty($post['foto3']) && strstr($post['foto3'],'data:image/jpeg;base64,')>-1){
                    $post['foto3'] = $this->uploadPhoto($post['foto3']);
                }else{
                    $post['foto3'] = $this->db->get_where('empresas',array('id'=>$id))->row()->foto3;
                }
                if(!empty($post['foto4']) && strstr($post['foto4'],'data:image/jpeg;base64,')>-1){
                    $post['foto4'] = $this->uploadPhoto($post['foto4']);
                }else{
                    $post['foto4'] = $this->db->get_where('empresas',array('id'=>$id))->row()->foto4;
                }
                $p = $post['ubicacion_geo'];
                $p = str_replace('(','',$p);
                $p = str_replace(')','',$p);
                $p = explode(',',$p);
                $post['lat'] = $p[0];
                $post['lon'] = $p[1];
                return $post;
            });
            $crud->set_relation_n_n('especialidades','empresas_especialidades','especialidades','empresas_id','especialidades_id','nombre_especialidad','priority');
            $crud->callback_column('facebook',function($val){$val = str_replace('http://','',$val); return 'http://'.$val;});
            $crud->callback_column('twitter',function($val){$val = str_replace('http://','',$val); return 'http://'.$val;});
            $crud->callback_column('linkedin',function($val){$val = str_replace('http://','',$val); return 'http://'.$val;});            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function link($val){
             if(strstr($val,'http://') || strstr($val,'https://')){
                    return $val;
                }else{
                    return 'http://'.$val;
                }
        }
        
        function uploadPhoto($img){
            $str="data:image/jpeg;base64,";
            $data=str_replace($str,"",$img); 
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            $name = rand(1024,5000).$_FILES["foto4"]["name"].'.jpeg';
            file_put_contents('/var/www/pronexapp.com/htdocs/newapp/images/fotos_empresas/'.$name, $data);
            return $name;
        }
        
        public function getEmpresas($x = '',$y = ''){
            $this->as = array('getEmpresas'=>'empresas');
            $crud = $this->crud_function('','');
            $crud->unset_add()
                    ->unset_edit()
                    ->unset_delete();
            $crud->callback_column('foto1',function($val){return base_url('images/fotos_empresas/'.$val);});
            $crud->callback_column('foto2',function($val){return base_url('images/fotos_empresas/'.$val);});
            $crud->callback_column('foto3',function($val){return base_url('images/fotos_empresas/'.$val);});
            $crud->callback_column('foto4',function($val){return base_url('images/fotos_empresas/'.$val);});
            $crud->callback_column('especialidades',function($val,$row){
                $e = array();
                foreach(get_instance()->db->get_where('empresas_especialidades',array('empresas_id'=>$row->id))->result() as $es){
                    $e[] = $es->id;
                }
                return json_encode($e);
            });            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
         public function searchEmpresas($x = '',$y = ''){
            if(!empty($_POST['search_field'])){
                foreach($_POST['search_field'] as $n=>$p){
                    switch($p){
                        case 'nombre':
                            foreach(explode(' ',$_POST['search_text'][$n]) as $b){
                                $this->db->or_like('empresas.nombre',$b);
                                $this->db->or_like('empresas.descripcion',$b);
                                $this->db->or_like('nombre_sector',$b);
                                $this->db->or_like('nombre_especialidad',$b);
                            }
                        break;
                        case 'especialidades':
                            foreach(explode(',',$_POST['search_text'][$n]) as $e){
                                $this->db->where('especialidades_id',$e);
                            }                                                    
                        break;
                        case 'sectores':
                            $this->db->where('sectores.id',$_POST['search_text'][$n]);                        
                        break;
                        case 'zip':
                            $this->db->where('zip',$_POST['search_text'][$n]);
                        break;
                        default:
                            $this->db->where('empresas.id',-1);
                        break; 
                    }
                }
            }
            
            $empresas = array(); 
            $this->db->join('sectores','sectores.id = empresas.sectores_id','left');
            $this->db->join('empresas_especialidades','empresas_especialidades.empresas_id = empresas.id','left');
            $this->db->join('especialidades','empresas_especialidades.especialidades_id = especialidades.id','left');
            $this->db->group_by('empresas.id');
            $this->db->select('empresas.*, sectores.nombre_sector as s986e880e, especialidades.nombre_especialidad');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $this->db->join('especialidades','empresas_especialidades.especialidades_id = especialidades.id');
                $e->nombre_especialidad = $this->db->get_where('empresas_especialidades',array('empresas_id'=>$e->id));
                if($e->nombre_especialidad->num_rows>0){
                    $e->nombre_especialidad = $e->nombre_especialidad->row()->nombre_especialidad;
                }else{
                    $e->nombre_especialidad = '';
                }
                $empresas[] = $e;
            }
            echo json_encode($empresas);
        }
        
        public function especialidades($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->order_by('nombre_especialidad','ASC');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function sectores($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function gestor($x = '',$y = ''){
            $_POST['fecha'] = date("Y-m-d",strtotime(str_replace('/','-',$_POST['fecha'])));
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function viewLands(){
            $this->form_validation->set_rules('lat','LATITUDE','required');
            $this->form_validation->set_rules('lon','LONGITUDE','required');
            $this->form_validation->set_rules('fecha','FECHA','required');
            if($this->form_validation->run()){
                $data = array();
                $sql='SELECT *,usuarios.id as usuarioid,  ( 3959 * acos( cos( radians('.$_POST['lat'].') ) * cos( radians( lat ) ) * cos( radians( lon ) - radians('.$_POST['lon'].') ) + sin( radians('.$_POST['lat'].') ) * sin( radians( lat ) ) ) ) AS distance FROM gestor LEFT JOIN usuarios on usuarios.id = gestor.usuarios_id where gestor.fecha >= \''.date("Y-m-d",strtotime(str_replace('/','-',$_POST['fecha']))).'\' HAVING distance < 25 ORDER BY distance LIMIT 0 , 20';
                foreach($this->db->query($sql)->result() as $q){
                    $data[] = $q;
                }
                echo json_encode($data);
            }
            else{
                echo json_encode(array());
            }
        }
        
        public function ranking(){
            $crud = $this->crud_function('','',$this);
            $crud->set_rules('usuarios_id','Usuario','required|integer|greather_than[0]|callback_if_user_rankeo');
            $crud->callback_before_insert(function($post){
                $datos = array();
                $datos['calidad'] = $post['calidad'];
                $datos['limpieza'] = $post['limpieza'];
                $datos['rapidez'] = $post['rapidez'];
                $datos['seriedad'] = $post['seriedad'];
                $datos['confianza'] = $post['confianza'];
                $datos['ranking'] = 0;
                $empresa = get_instance()->db->get_where('empresas',array('id'=>$post['empresas_id']))->row();
                $rankeo = get_instance()->db->get_where('ranking',array('empresas_id'=>$empresa->id))->num_rows+1;
                foreach($datos as $n=>$v){
                    $datos[$n]+=$empresa->$n;
                    $datos['ranking']+=$datos[$n];
                }
                $datos['ranking']+= $empresa->ranking;
                $datos['ranking'] = $datos['ranking']/(5*$rankeo);
                get_instance()->db->update('empresas',$datos,array('id'=>$empresa->id));
                return true;
                
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function if_user_rankeo(){
            if($this->db->get_where('ranking',array('empresas_id'=>$_POST['empresas_id'],'usuarios_id'=>$_POST['usuarios_id']))->num_rows>0){
               // $this->form_validation->set_message('if_user_rankeo','Usted ya ha rankeado a esta empresa');
                $data = array(
                    'seriedad'=>$_POST['seriedad'],
                    'confianza'=>$_POST['confianza'],
                    'rapidez'=>$_POST['rapidez'],
                    'limpieza'=>$_POST['limpieza'],
                    'calidad'=>$_POST['calidad']
                );
                $this->db->update('ranking',$data,array('empresas_id'=>$_POST['empresas_id'],'usuarios_id'=>$_POST['usuarios_id']));
                $this->form_validation->set_message('if_user_rankeo','Su ranking se ha realizado satisfactoriamente');
                return false;
            }
            else{
                return true;
            }
        }
        
        public function getFromDistance(){
            $lat = empty($_POST['search_text'][0])?40.41373548106751:$_POST['search_text'][0];
            $lon = empty($_POST['search_text'][1])?-3.706865632226595:$_POST['search_text'][1];
            $query = 'SELECT empresas.*, sectores.nombre_sector, ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lon ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM empresas JOIN sectores on sectores.id = empresas.sectores_id ORDER BY distance LIMIT 0,4';
            $empresas = array();
            foreach($this->db->query($query)->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $e->distance = round($e->distance/0.62137,1);
                $empresas[0][0][] = $e;
            }
            
            $query = 'SELECT empresas.*, sectores.nombre_sector,  ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lon ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM empresas JOIN sectores on sectores.id = empresas.sectores_id ORDER BY distance LIMIT 4,4';
            foreach($this->db->query($query)->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $e->distance = round($e->distance/0.62137,1);
                $empresas[0][1][] = $e;
            }
            
            $query = 'SELECT empresas.*, sectores.nombre_sector,  ( 3959 * acos( cos( radians('.$lat.') ) * cos( radians( lat ) ) * cos( radians( lon ) - radians('.$lon.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance FROM empresas JOIN sectores on sectores.id = empresas.sectores_id ORDER BY distance LIMIT 8,4';
            foreach($this->db->query($query)->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $e->distance = round($e->distance/0.62137,1);
                $empresas[0][2][] = $e;
            }
            
            $this->db->order_by('ranking','DESC');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->limit('4','0');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[1][0][] = $e;
            }
            
            $this->db->order_by('ranking','DESC');
            $this->db->limit('4','4');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[1][1][] = $e;
            }
            
            $this->db->order_by('ranking','DESC');
            $this->db->limit('8','4');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[1][2][] = $e;
            }
            
            $this->db->order_by('contacts','DESC');
            $this->db->limit('4','0');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[2][0][] = $e;
            }
            
            $this->db->order_by('contacts','DESC');
            $this->db->limit('4','4');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[2][1][] = $e;
            }
            
            $this->db->order_by('contacts','DESC');
            $this->db->limit('8','4');
            $this->db->join('sectores','sectores.id = empresas.sectores_id');
            $this->db->select('empresas.*, sectores.nombre_sector');
            foreach($this->db->get('empresas')->result() as $e){
                $e->foto1 = base_url('images/fotos_empresas/'.$e->foto1);
                $e->foto2 = base_url('images/fotos_empresas/'.$e->foto2);
                $e->foto3 = base_url('images/fotos_empresas/'.$e->foto3);
                $e->foto4 = base_url('images/fotos_empresas/'.$e->foto4);
                $empresas[2][2][] = $e;
            }
            echo json_encode($empresas);
        }
        
        function recuperarPass(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $email = $this->db->get_where('usuarios',array('email'=>$_POST['email']));
                if($email->num_rows>0){
                    $newPass = rand(10000000,90000000);
                    $this->db->update('usuarios',array('password'=>$newPass),array('email'=>$_POST['email']));
                    $email = $email->row();
                    $email->password = $newPass;
                    $this->sendMail($email,2);
                    echo 'Su contraseña se ha renovado, dirigase a su correo para continuar con el proceso de renovación.';
                }
                else{
                    echo 'El email ingresado no se encuentra registrado';
                }
            }
            else{
                echo 'Se debe indicar un Email Válido';
            }
        }
        
        function condiciones(){
            echo $this->db->get_where('notificaciones',array('id'=>3))->row()->texto;
        }
        
        function sendMail($usuario,$idnotificacion){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            $mensaje->texto = str_replace('{usuarios.nombre}',$usuario->nombre,$mensaje->texto);
            $mensaje->titulo = str_replace('{usuarios.nombre}',$usuario->nombre,$mensaje->titulo);
            $mensaje->texto = str_replace('{usuarios.password}',$usuario->password,$mensaje->texto);
            correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            correo('joncar.c@gmail.com',$mensaje->titulo,$mensaje->texto);
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
