//var url = 'http://localhost/proyectos/mariachis/api/';
var url = 'http://www.pronexapp.com/newapp/api/';
var myPopup;
angular.module('starter.services', [])

.factory('Api', function() {
  var email = '';
  var password = '';
  var data = "";
  return {
    list:function(controller,data,$scope,$http,successFunction,operator){        
        if(typeof(data)=='object'){
            s = '';            
            for(i in data){
                s+= 'search_field[]='+i+'&search_text[]='+data[i]+'&';
            }
            data = s;
            data+= operator==undefined?'':'operator='+operator;
            data+='&usuarios_id='+$scope.user;
        }
        if(typeof($scope.loading)!=='undefined'){
            $scope.loading('hide');
        }
        $http({
            url:url+controller+'/json_list',
            method: "POST",
            data:data,
            transformRequest:angular.identity,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
          }).success(function(data){              
             if(successFunction!==undefined){
                 successFunction(data);
                 if(typeof($scope.loading)!=='undefined'){
                     $scope.loading('hide');
                }
             }
          })
        .catch(function(data, status, headers, config){ // <--- catch instead error
            if(typeof($scope.loading)!=='undefined'){
                    $scope.loading('hide');
               }
            //alert('{'+JSON.stringify(data)+'} {'+JSON.stringify(headers)+'}'); //contains the error message
            alert('Ocurrio un error al capturar los datos del servidor');
        });
    },
    insert:function(controller,$scope,$http,successFunction){       
       if(!$scope.data){//Si ls datos son de un formulario
            d = document.getElementById('formreg');
            data = new FormData(d);  
        }//Si se envia el array
        else{
            data = new FormData();
            for(i in $scope.data){
              data.append(i,$scope.data[i]);
            }            
        }
        $scope.loading('show');     
       $http({
            url:url+controller+'/insert_validation',
            method: "POST",
            data:data,
            transformRequest: angular.identity,
            headers: {
              'Content-Type':undefined
            }           
          }).success(function(data){
              data = data.replace('<textarea>','');
              data = data.replace('</textarea>','');
              data = JSON.parse(data);
              if(data.success){
                  if(!$scope.data){//Si ls datos son de un formulario
                        d = document.getElementById('formreg');
                        data = new FormData(d);  
                  }//Si se envia el array
                  else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }            
                  }       
                  $http({
                        url:url+controller+'/insert',
                        method: "POST",
                        data:data,
                        transformRequest: angular.identity,
                        headers: {
                          'Content-Type':undefined
                        }           
                      }).success(function(data){
                          data = data.replace('<textarea>','');
                          data = data.replace('</textarea>','');
                          data = JSON.parse(data);                          
                          if(data.success){
                               if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                               }
                          }
                          else{
                             $scope.loading('hide');
                             $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                          }
                        })
                        .error(function(){
                            alert('Ha ocurrido un error interno, contacte con un administrador');
                        });
              }
              else{
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al añadir',data.error_message);
              }
          })
          .error(function(data){
              alert('Ha ocurrido un error interno, contacte con un administrador');
          });
        },
        
        update:function(controller,id,$scope,$http,successFunction){
        if(!$scope.data){//Si ls datos son de un formulario
          d = document.getElementById('formreg');
          data = new FormData(d);
        }//Si se envia el array
        else{
          data = new FormData();
          for(i in $scope.data){
            data.append(i,$scope.data[i]);
          }
        }
        $scope.loading('show');            
        $http({
             url:url+controller+'/update_validation/'+id,
             method: "POST",
             data:data,
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               data = data.replace('<textarea>','');
               data = data.replace('</textarea>','');
               data = JSON.parse(data);
               if(data.success){
                    if(!$scope.data){//Si ls datos son de un formulario
                      d = document.getElementById('formreg');
                      data = new FormData(d);
                    }//Si se envia el array
                    else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }
                    }       
                   $http({
                         url:url+controller+'/update/'+id,
                         method: "POST",
                         data:data,
                         transformRequest: angular.identity,
                         headers: {
                           'Content-Type':undefined
                         }           
                       }).success(function(data){
                           data = data.replace('<textarea>','');
                           data = data.replace('</textarea>','');
                           data = JSON.parse(data);                          
                           if(data.success){
                                if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                                }
                           }
                           else{
                              $scope.loading('hide');
                              $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                           }
                    });
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al añadir',data.error_message);
               }
          });
        },
        
        deleterow:function(controller,id,$scope,$http,successFunction){        
        $scope.loading('show');
        $http({
             url:url+controller+'/delete/'+id,
             method: "GET",
             data:'',
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               
               if(data.success){
                    if(successFunction!==undefined){
                        successFunction(data);
                        $scope.loading('hide');
                     }
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al eliminar',data.error_message);
               }
          });
        },

        initInterface:function($scope){

          return $scope;
        },

        query:function(controller,$scope,$http,successFunction){
            if(!$scope.data){//Si ls datos son de un formulario
              d = document.getElementById('formreg');
              data = new FormData(d);
            }
            else{
                data = new FormData();
                for(i in $scope.data){
                  data.append(i,$scope.data[i]);
                }
            }            
            
            if($scope.loading!=undefined){
                $scope.loading('show');
            }
            $http({
                 url:url+controller,
                 method: "POST",
                 data:data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':undefined
                 }           
               }).success(function(data){
                    if(successFunction!==undefined){
                        successFunction(data);
                        if($scope.loading!=undefined){
                            $scope.loading('hide');
                        }
                     }
              }).error(function(data){
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al enviar los datos',data);
              });
        },
        
        remote:function(url,type,$scope,$http,successFunction){            
            $http({
                 url:url,
                 method: type,
                 data:$scope.data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':undefined
                 }           
               }).success(function(data){
                  if(successFunction!==undefined){
                    successFunction(data);                            
                 }
              });
        }
    }   
})

.factory('UI', function() {
    return {    
        showPopupBox:function($ionicPopup,$scope,message,title,buttonAccept){
            $scope.data = {}
            // An elaborate, custom popup
              myPopup = $ionicPopup.show({
              template: message,
              title: title,              
              scope: $scope,
              buttons: [
                { 
                 text: 'Cancelar',
                 onTap:function(e){
                     myPopup.close();
                 }                 
                },
                buttonAccept
              ]
            });
            myPopup.then(function(res) {
              console.log('Tapped!', res);
            });
            
            /*ButtonAccept = {text: buttonAccept['text'],
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.wifi) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                return $scope.data.wifi;
              }
            }
          }}*/
        },
        getShowAlert:function($ionicPopup){
            
            return function(title,template){
                if(this.alertMostrado===undefined){                                    
                    var scope = this;
                    this.alertMostrado = $ionicPopup.show({
                    template: template,
                    title: title,                
                    buttons: [                  
                      {
                        text: '<b>Aceptar</b>',
                        type: 'button-positive',
                        onTap: function(e) {                      
                          scope.alertMostrado.close();
                          scope.alertMostrado = undefined;
                        }
                      }
                    ]
                  });
                };
            };
        },
        getLoadingBox:function($ionicLoading,message){
            message = message==undefined?'Cargando...':message;
            return function(attr){            
                attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: message});
            };
        }, 
        
        getConfirmBox:function($ionicPopup) {
           return function(title,template,acceptFunction,denyFunction){
               var confirmPopup = $ionicPopup.confirm({
                    title: title,
                    template: template,
                    cancelType:'button-assertive'
                  });
                  confirmPopup.then(function(res) {
                    if(res) {
                      if(acceptFunction)acceptFunction();
                    } else {
                      if(denyFunction)denyFunction();
                    }
                  });
            };
        },
        
        getModalBox:function($ionicModal,template,$scope,callback){
            $ionicModal.fromTemplateUrl(template, {//Invitar personas
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
                if(typeof(callback)!=='undefined'){
                    callback(modal);
                }
              });

            $scope.toggleModal = function(attr) {
              if(attr==undefined || attr=='show'){
                  $scope.modal.show();
              }
              else{
                  $scope.modal.remove();    
              }
            };            
            
            $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {                
                $scope.toggleModal('hide');
            });
            
            return $scope;
        }
    }
})

.factory('User', function() {  
  return {
        setData:function(data){            
            localStorage.user = data.id;
            localStorage.nombre = data.nombre;            
            localStorage.email = data.email;            
            localStorage.telefono = data.telefono;  
        },
        
        getData:function($scope){
            $scope.user = localStorage.user;
            $scope.email = localStorage.email;        
            $scope.nombre = localStorage.nombre;                  
            $scope.telefono = localStorage.telefono;                  
            return $scope;
        },
        
        cleanData:function(){
            localStorage.removeItem('user');
            localStorage.removeItem('nombre');            
            localStorage.removeItem('email');            
            localStorage.removeItem('telefono');            
            localStorage.removeItem('misempresas');
        }
    }  
});