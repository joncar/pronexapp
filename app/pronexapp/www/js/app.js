// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });
})

.config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.position('bottom');
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    cache:true,
    templateUrl: 'templates/tabsMain.html',
    controller:'Menu'
  })

  // Each tab has its own nav history stack:

  .state('tab.main', {
    url: '/main',
    cache:false,
    views: {
      'tab-main': {
        templateUrl: 'templates/main.html',
        controller: 'Main'
      }
    }
  })
  
  .state('tab.cuenta', {
    url: '/cuenta',
    cache:false,
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/cuenta.html',
        controller: 'Cuenta'
      }
    }
  })
  
  .state('tab.actualizarDatos', {
    url: '/actualizarDatos',
    cache:false,
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/Registrar.html',
        controller: 'actualizarDatos'
      }
    }
  })
  
  .state('tab.Misempresas', {
    url: '/misempresas',
    cache:false,
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/mis-empresas.html',
        controller: 'Misempresas'
      }
    }
  })
  
  .state('tab.Crearempresa', {
    url: '/miempresa',
    cache:false,
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/mi-empresa.html',
        controller: 'Miempresa'
      }
    }
  })
  
  .state('tab.Miempresa', {
    url: '/miempresa/:id',
    cache:false,
    views: {
      'tab-cuenta': {
        templateUrl: 'templates/mi-empresa.html',
        controller: 'Miempresa'
      }
    }
  })
  
  .state('tab.Empresas', {
    url: '/empresas',
    cache:false,
    views: {
      'tab-empresas': {
        templateUrl: 'templates/empresas.html',
        controller: 'Empresas'
      }
    }
  })
  
.state('tab.detalleEmpresa', {
    url: '/detalleEmpresa/:id',
    cache:false,
    views: {
      'tab-empresas': {
        templateUrl: 'templates/empresa-detail.html',
        controller: 'EmpresaDetail'
      }
    }
  })
  
  .state('tab.detalleEmpresaMore', {
    url: '/detalleEmpresaMore/:id',
    cache:false,
    views: {
      'tab-empresas': {
        templateUrl: 'templates/empresa-detail-more.html',
        controller: 'EmpresaDetail'
      }
    }
  })
  
  .state('tab.mapa', {
    url: '/mapa/:id',
    cache:false,
    views: {
      'tab-empresas': {
        templateUrl: 'templates/empresa-detail-mapa.html',
        controller: 'EmpresaDetail'
      }
    }
  })
  
  .state('tab.valorar', {
    url: '/valorar/:id',
    cache:false,
    views: {
      'tab-empresas': {
        templateUrl: 'templates/empresa-valorar.html',
        controller: 'EmpresaDetail'
      }
    }
  })
  
  .state('tab.gestor', {
    url: '/gestor',
    cache:false,
    views: {
      'tab-gestor': {
        templateUrl: 'templates/gestor.html',
        controller: 'Gestor'
      }
    }
  })
  
.state('tab.gestorTierra', {
    url: '/gestorTierra',
    cache:false,
    views: {
      'tab-gestor': {
        templateUrl: 'templates/gestorTierra.html',
        controller: 'GestorTierra'
      }
    }
  })
  
.state('tab.addTierra', {
    url: '/addTierra',
    cache:false,
    views: {
      'tab-gestor': {
        templateUrl: 'templates/addTierra.html',
        controller: 'addTierra'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/main');

});
