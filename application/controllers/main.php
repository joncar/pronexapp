<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();
class Main extends CI_Controller {
    
	public function __construct()
	{
            parent::__construct();
            $this->load->helper('url');
            $this->load->helper('html');
            $this->load->helper('h');
            $this->load->database();
            $this->load->model('user');
	}
        
        public function index()
	{
            $log = $this->user;
            if($log->log)
            $this->loadView('panel');
            else
            $this->loadView('main');                
	}
        
        public function success($msj)
	{
		return '<div class="alert alert-success">'.$msj.'</div>';
	}

	public function error($msj)
	{
		return '<div class="alert alert-danger">'.$msj.'</div>';
	}
        
        public function login()
	{
		if(!$this->user->log)
		{	
			if(!empty($_POST['email']) && !empty($_POST['pass']))
			{
				$this->db->where('email',$this->input->post('email'));
				$r = $this->db->get('user');
				if($this->user->login($this->input->post('email',TRUE),$this->input->post('pass',TRUE)))
				{
					if($r->num_rows>0 && $r->row()->status==1)
					{
                                            if(!empty($_POST['remember']))$_SESSION['remember'] = 1;
                                            if(empty($_POST['redirect']))
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.site_url('panel').'"</script>');
                                            else
                                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="'.$_POST['redirect'].'"</script>');
					}
					else $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
				}
                                else $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
			}
			else
                            $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
                        
                        if(!empty($_SESSION['msj']))
                            header("Location:".base_url());
		}
                else header("Location:".base_url());
	}
        
        function pages($titulo){
            $titulo = urldecode(str_replace("-","+",$titulo));
            if(!empty($titulo)){
                $pagina = $this->db->get_where('paginas',array('titulo'=>$titulo));
                if($pagina->num_rows>0){
                    $this->loadView(array('view'=>'paginas','contenido'=>$pagina->row()->contenido,'title'=>$titulo));                    
                }
                else $this->loadView('404');
            }
        }

	public function unlog()
	{
		$this->user->unlog();                
                header("Location:".site_url());
	}
        
        public function loadView($param = array('view'=>'main'))
        {
            if(is_string($param))
            $param = array('view'=>$param);
            $this->load->view('template',$param);
        }
		
	public function loadViewAjax($view,$data = null)
        {
            $view = $this->valid_rules($view);
            $this->load->view($view,$data);
        }
        
         function error404(){
            $this->loadView(array('view'=>'errors/403'));
        }
        
        function app()
        {            
            $this->loadView(array('view'=>'mobile'));
        }
}
