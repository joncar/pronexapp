<?php 
    require_once APPPATH.'/controllers/panel.php';    
    class App extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function especialidades(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function sectores(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function usuarios(){
            $crud = $this->crud_function('','');
            $crud->field_type('password','password');
            $crud->set_field_upload('foto','images/fotos_usuarios');
            $crud->set_rules('email','required','required|valid_email')
                 ->set_rules('password','password','required|min_length[8]');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function empresas(){
            $crud = $this->crud_function('','');
            $crud->set_rules('email','required','required|valid_email');
            $crud->field_type('ubicacion_geo','map',array());
            $crud->set_field_upload('foto1','images/fotos_empresas');
            $crud->set_field_upload('foto2','images/fotos_empresas');
            $crud->set_field_upload('foto3','images/fotos_empresas');
            $crud->set_field_upload('foto4','images/fotos_empresas');
            $crud->set_relation_n_n('especialidades','empresas_especialidades','especialidades','empresas_id','especialidades_id','nombre_especialidad','priority');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
