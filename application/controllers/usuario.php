<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('admin.php');
class Usuario extends Admin {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('registrarse/add?redirect='.$_SERVER['REQUEST_URI']));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');
                ini_set('date.timezone', 'America/Caracas');
                date_default_timezone_set('America/Caracas');                                
	}
       
        public function index($url = 'main',$page = 0)
	{		
              parent::index();
	}
        
        function adquirir_licencia(){
             $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('adquirir_licencia',array(),TRUE)));
        }
        
        function depositos_pagina($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print();
            $crud->where('user',$_SESSION['user']);
            $crud->fields('user','tipo','banco','fecha','nro_comprobante','monto','imagen','nodo');
            $crud->field_type('user','hidden',$_SESSION['user'])
                 ->field_type('nodo','hidden',$_SESSION['nodo'])
                 ->field_type('tipo','dropdown',array('Deposito'=>'Deposito','Transferencia'=>'Transferencia'));
            
            
            if($_SESSION['registro']==0){
                $crud->callback_field('monto',function($val,$row){
                    $this->db->select('nodos.*, planes.costo as costo_apoyo_dineroenlinea');
                    $this->db->join('planes','planes.id = nodos.plan');
                    $nodo = $this->db->get_where('nodos',array('nodos.id'=>$_SESSION['nodo']));
                    if($nodo->num_rows>0)
                    return form_input('monto',$nodo->row()->costo_apoyo_dineroenlinea,'id="field-monto" readonly');
                });
            }
            else{
                $crud->callback_field('monto',function($val,$row){
                    $this->db->select('nodos.*, planes.costo as costo_apoyo_dineroenlinea');
                    $this->db->join('planes','planes.id = nodos.plan');
                    $nodo = $this->db->get_where('nodos',array('nodos.id'=>$_SESSION['nodo']));
                    if($nodo->num_rows>0)
                    {
                        $monto = $nodo->row()->costo_apoyo_dineroenlinea;
                        $asociado = $this->querys->get_asociado();
                        $monto += $this->db->get_where('planes',array('id'=>$asociado->plan))->row()->costo;
                        $asociado = $this->querys->get_asociado('asociado6x1');
                        $monto += $this->db->get_where('planes',array('id'=>$asociado->plan))->row()->costo;
                    }
                    return form_input('monto',$monto,'id="field-monto" readonly');
                });
                
                $crud->callback_after_insert(function($post,$id){
                    $monto = $post['monto']/3;
                    $asociado = $this->querys->get_asociado();
                    $asociado6x1 = $this->querys->get_asociado6x1();
                    $detalle = $asociado->email_asoc.' || Realizar deposito a nombre de:'.$asociado->nombre.' '.$asociado->apellido_paterno.' Banco: '.$asociado->banco.' | nro. Cuenta: '.$asociado->cuenta_bancaria;
                    $this->db->insert('depositos_asociados',array('nodo'=>$_SESSION['nodo'],'user'=>$_SESSION['user'],'deposito_express'=>$id,'asociado'=>$asociado->userasoc,'monto'=>$monto,'status'=>-2,'tipo_asociado'=>'asociado','fecha'=>date("Y-m-d"),'detalle'=>$detalle));
                    $detalle = $asociado6x1->email_asoc.' || Realizar deposito a nombre de:'.$asociado6x1->nombre.' '.$asociado6x1->apellido_paterno.' Banco: '.$asociado6x1->banco.' | nro. Cuenta: '.$asociado6x1->cuenta_bancaria;
                    $this->db->insert('depositos_asociados',array('nodo'=>$_SESSION['nodo'],'user'=>$_SESSION['user'],'deposito_express'=>$id,'asociado'=>$asociado6x1->userasoc,'monto'=>$monto,'status'=>-2,'tipo_asociado'=>'6x1','fecha'=>date("Y-m-d"),'detalle'=>$detalle));
                });
            }
            
            
            
            $crud->display_as('imagen','imagen o pdf');
            $crud->set_field_upload('imagen','files');
            $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado'));
            $crud->unset_columns('user');
            $crud->set_relation('banco','bancos','{nombre} {nro_cuenta} {tipo}');
            if($this->querys->get_level()!=4)
                $crud->unset_back_to_list();
            $output = $crud->render();
            $output->crud = 'depositos_pagina';
            $this->loadView($output);
        }
        
        function depositos_asociados($x = '', $y = '')
        {
            $crud = parent::crud_function($x,$y);
            $crud->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->unset_fields('status','deposito_express');            
            $crud->field_type('user','hidden',$_SESSION['user'])  
                 ->field_type('detalle','invisible')                 
                 ->field_type('tipo','dropdown',array('Deposito'=>'Deposito','Transferencia'=>'Transferencia'));
            $crud->where('user',$_SESSION['user']);
            
            
            $crud->display_as('imagen','imagen o pdf');
            $crud->set_field_upload('imagen','files');
            
            if($x=='6x1' && $_SESSION['user']!=1){
                $crud->display_as('asociado','Enviar dinero a un asignado: ');
                $asociado = 'asociado6x1';
                $crud->field_type('tipo_asociado','hidden','6x1');
                $crud->callback_field('monto',function($val,$row){
                    $asociado = get_instance()->db->get_where('nodos',array('id'=>$_SESSION['nodo']))->row();
                    $asociado = get_instance()->db->get_where('nodos',array('user'=>$asociado->asociado6x1,'plan'=>$asociado->plan))->row();
                    return form_input('monto',$this->db->get_where('planes',array('id'=>$asociado->plan))->row()->costo,'id="field-monto" readonly');
                });                
                $crud->callback_field('asociado',function(){
                    $asociado = $this->querys->get_asociado('asociado6x1');                
                    return '<input type="text" name="detalle" id="field-detalle" class="form-control" value="'.$asociado->email_asoc.' || Realizar deposito a nombre de:'.$asociado->nombre.' '.$asociado->apellido_paterno.' Banco: '.$asociado->banco.' | nro. Cuenta: '.$asociado->cuenta_bancaria.'" readonly> <input type="hidden" name="asociado" id="field-asociado" value="'.$asociado->asociado6x1.'">';
                });
            }
            
            else if($_SESSION['user']!=1){
                $crud->display_as('asociado','Enviar dinero a tu anfitrion: ');
                $asociado = 'asociado';
                $crud->callback_field('asociado',function(){
                    $asociado = $this->querys->get_asociado();
                    return '<input type="text" name="detalle" id="field-detalle" class="form-control" value="'.$asociado->email_asoc.' || Realizar deposito a nombre de:'.$asociado->nombre.' '.$asociado->apellido_paterno.' Banco: '.$asociado->banco.' | nro. Cuenta: '.$asociado->cuenta_bancaria.'" readonly> <input type="hidden" name="asociado" id="field-asociado" value="'.$asociado->asociado.'">';
                });
                $crud->field_type('tipo_asociado','hidden','asociado');
                $crud->callback_field('monto',function($val,$row){
                    $asociado = get_instance()->db->get_where('nodos',array('id'=>$_SESSION['nodo']))->row();                
                    $asociado = get_instance()->db->get_where('nodos',array('user'=>$asociado->asociado,'plan'=>$asociado->plan))->row();                
                    return form_input('monto',$this->db->get_where('planes',array('id'=>$asociado->plan))->row()->costo,'id="field-monto" readonly');
                });
            }
            $crud->field_type('nodo','hidden',$_SESSION['nodo']);
            if(empty($x) || ($x=='6x1' && empty($y)) || $x=='success' || $x=='ajax_list'){
                $crud->set_relation('asociado','user','{nombre} {apellido_paterno}');
                $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado','2'=>'Validado por dineroenlinea'));
            }
            if($this->querys->get_level()!=4)
                $crud->unset_back_to_list();
            $crud->unset_columns('user','tipo_asociado','nodo');
            $output = $crud->render();
            $output->crud = 'depositos_asociados';           
            $this->loadView($output);
        }
        
        function validar_pagos($x = '',$y = ''){
            if($x=='edit' && !empty($y) && $this->db->get_where('depositos_asociados',array('id'=>$y))->row()->status==-2)
                header("Location:".base_url('usuario/validar_pagos'));
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('bootstrap');
            $crud->set_table('depositos_asociados');
            $crud->set_subject('deposito');
            $crud->where('depositos_asociados.asociado',$_SESSION['user']);
            $crud->set_field_upload('imagen','files');
            $crud->unset_columns('asociado','deposito_express')
                 ->unset_delete()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
            if($_SESSION['semaforo']==5){
                $crud->unset_edit();
            }
            $crud->fields('status');
            if(empty($x) || $x=='ajax_list')
            $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado','-2'=>'Esperando liberación de dineroenlinea'));
            else
            $crud->field_type('status','dropdown',array('0'=>'Por validar','1'=>'Validado','-1'=>'Rechazado'));
            $crud->set_relation('user','user','{nombre} {apellido_paterno}');
            $output = $crud->render();            
            $this->loadView($output);
            
        }
        
        function invitados($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);
            $crud->set_theme('bootstrap');
            $crud->where('user',$_SESSION['user']);
            
            $crud->unset_columns('user')
                 ->field_type('user','hidden',$_SESSION['user'])
                 ->field_type('fecha','hidden',date("Y-m-d"))
                 ->unset_edit()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]|is_unique[invitados.email]');
            $output = $crud->render();    
            $str = 'Tu link de referido es: <b>'.base_url('registrarse/'.$_SESSION['usuario'].'/add').'</b>';
            $output->output = $str.$output->output;
            $this->loadView($output);
            
        }
        
        function licencias($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);
            $crud->where('licencias.user',$_SESSION['user']);
            $crud->required_fields('');
            $crud->unset_columns('user')                 
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()                 
                 ->unset_edit();
            $crud->set_relation('asociado','user','{nombre} {apellido_paterno}',array('id !='=>1,'id != '=>$_SESSION['user']));
            $crud->set_relation('plan','planes','{nombre} {costo}');
            $crud->field_type('user','hidden',$_SESSION['user'])
                 ->field_type('codigo','invisible')
                 ->field_type('fecha_activacion','invisible')
                 ->field_type('fecha_vencimiento','invisible')
                 ->field_type('status','invisible')
                 ->field_type('grupo_limite','invisible')                 
                 ->field_type('restante','invisible');
            $crud->set_rules('asociado','Asociado','required|callback_can_fill');
            $crud->set_rules('plan','Plan','required|integer|greather_thah[0]|callback_can_get_6x1');
            $crud->callback_before_insert(function($post){
                $post['grupo_limite'] = 3;
                $post['restante'] = 3;
                return $post;
            });
            
            if($_SESSION['semaforo']==5){
                $crud->unset_add();
            }
            
            $crud->callback_after_insert(array($this,'licencias_ainsertion'));            
            if(empty($x) || $x=='ajax_list' || $x=='success')
                $crud->field_type('status','true_false',array('0'=>'Inactiva','1'=>'Activa'));     
            $crud->set_lang_string('form_save','Solicitar una nueva');
            $crud->unset_back_to_list();
            $output = $crud->render();                            
            $this->loadView($output);
            
        }
        
        function creditos($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);
            $crud->where('creditos.user',$_SESSION['user']);
            $crud->where('creditos.licencia',$_SESSION['licencia']);            
            $crud->unset_columns('user')
                 
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_add()
                 ->unset_edit();          
            $crud->callback_before_delete(function($id){
                if($this->db->get_where('creditos',array('id'=>$id,'user'=>$_SESSION['user'],'licencia'=>$_SESSION['licencia'],'status'=>0))->num_rows>0)
                    return true;
                else return false;
            });
            $crud->field_type('status','dropdown',array('0'=>'Esperando liberación','1'=>'Credito liberado y activo','2'=>'Credito Pagado'));
            $output = $crud->render();                 
            $this->loadView($output);           
        }
        //Esta funcion tambien esta en controller registro
        function can_fill($id){
            $this->db->join('user','user.id = licencias.user');
            $licencias = $this->db->get_where('licencias',array('user.id'=>$id,'licencias.restante >'=>0,'licencias.plan'=>$_POST['plan']));
            if($licencias->num_rows==0){
                  $u = $this->db->get_where('user',array('id'=>$id))->row();
                  if($u->cuenta==1){
                    //Si es usuario inicial u admin se le asigna la licencia automaticamente
                    $this->db->insert('licencias',array('user'=>$u->id,'grupo_limite'=>3,'restante'=>3,'status'=>1,'fecha_activacion'=>date("Y-m-d"),'asociado'=>1));
                    //Creamos el nuevo nodo
                    $this->db->update('licencias',array('codigo'=>substr(md5($this->db->insert_id()),0,8)),array('id'=>$this->db->insert_id()));
                    
                    $data['user'] = $u->id;
                    $data['licencia'] = $this->db->insert_id();
                    $data['usuario'] = $u->usuario;
                    $data['email'] = $u->email;
                    $data['asociado'] = 1;
                    $data['asociado_licencia'] = 1;
                    $data['asociado_email'] = 'root@dineroenlinea.com.mx';
                    $data['asociado6x1'] = 1;
                    $data['semaforo'] = 0;
                    $data['plan'] = 1;
                    $this->db->insert('nodos',$data);
                    return true;
                }
                else{
                    $this->form_validation->set_message('can_fill','El usuario ingresado no posee licencias disponibles para abarcarlo');
                    return false;
                }
            }
            return true;
        }
        
        //Esto valida si existen 2 asociados iniciales con el plan seleccionado para darlo como 6x1
        function can_get_6x1($id){
            $asociados = $this->db->get_where('nodos',array('asociado'=>1,'plan'=>$id));
            if($asociados->num_rows>=2){
                return true;
            }
            else{
                $this->form_validation->set_message('can_get_6x1','Existe un error en la plataforma, estamos trabajando para solucionarlo, intente más tarde.');
                return false;
            }
        }
        
        public function inscribir(){
            if(empty($_POST)){
            $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('inscribir',array(),TRUE)));
            }
            else{
                $notdata = array('licencia','plan','rfc');
                foreach($_POST as $n=>$p)
                {
                    if(!is_array($p) && $n!='sucursal')
                        $this->form_validation->set_rules($n,$n,'required');
                }                
                
                if($this->form_validation->run()){
                    $notdata = array('licencia','plan');
                    $data = array();
                    foreach($_POST as $n=>$p){
                    if(!is_array($p) && !in_array($n,$notdata))$data[$n] = $p;}
                                        
                    $this->db->update('user',$data,array('id'=>$_SESSION['user']));
                    $this->db->update('nodos',array('plan'=>$_POST['plan']),array('id'=>$_SESSION['nodo']));                    
                    echo json_encode (array('status'=>TRUE,'message'=>$this->form_validation->error_string()));
                }
                else echo json_encode (array('status'=>FALSE,'message'=>$this->form_validation->error_string()));
            }
        }
        
        public function change($id = ''){
            if(!empty($id)){
                $this->db->select('nodos.*');
                $this->db->join('nodos','nodos.licencia = licencias.id');
                $l = $this->db->get_where('licencias',array('licencias.user'=>$_SESSION['user'],'licencias.id'=>$id));
                if($l->num_rows>0){
                    $l = $l->row();
                    $_SESSION['asociado6x1'] = $l->asociado6x1;
                    $_SESSION['asociado'] = $l->asociado;
                    $_SESSION['licencia'] = $l->licencia;
                    $_SESSION['nodo'] = $l->id;
                    $_SESSION['semaforo'] = $l->semaforo;
                    $_SESSION['registro'] = $l->tipo_registro;
                }
            }
            if(empty($_GET['redirect']))
            header("Location:".base_url('panel'));
            else header("Location:".$_GET['redirect']);
        }
        
        function simulador_credito(){
            if(empty($_POST)){
                $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->load->view('simulador_credito',array(),TRUE)));
            }
            else{
                if($_SESSION['semaforo']==2 && ($this->db->get_where('creditos',array('user'=>$_SESSION['user'],'licencia'=>$_SESSION['licencia']))->num_rows==0 || $this->db->get_where('creditos',array('user'=>$_SESSION['user'],'licencia'=>$_SESSION['licencia'],'status <'=>2))->num_rows==0)){
                    foreach($_POST as $n=>$p)
                      $this->form_validation->set_rules($n,$n,'required');
                      if($this->form_validation->run()){
                        $data = array();
                        $data['user'] = $_SESSION['user'];
                        $data['licencia'] = $_SESSION['licencia'];
                        $data['monto'] = $_POST['monto'];
                        $data['dias'] = $_POST['dias'];
                        $data['fecha'] = date("Y-m-d");
                        $data['fecha_pago'] = date("Y-m-d",strtotime('+'.$_POST['dias'].' days '.date("Y-m-d")));
                        $data['pago_capital'] = $_POST['monto'];
                        $data['interes_neto'] = ($_POST['interes_diario']/100)*$_POST['dias'];
                        $data['interes_ordinario'] = $data['interes_neto']/1.16;
                        $data['iva_interes'] = $data['interes_neto']-$data['interes_ordinario'];
                        $data['total_pagar'] = $_POST['interes'];
                        $data['total_pagado'] = 0;
                        $this->db->insert('creditos',$data);
                        echo $this->success('credito solicitado con éxito. A la espera de su aprobación');
                    }
                    else echo $this->error($this->form_validation->error_string());
                }
                else{
                    echo $this->error('Esta licencia no esta autorizada para solicitar créditos. <a href="'.base_url('usuario/adquirir_licencia').'" class="btn btn-default">Conoce como adquirir una licencia aqui</a>');
                }
            }
        }
        
        function red(){
            $this->loadView(array('view'=>'cruds/user','output'=>$this->load->view('arbol',array(),TRUE)));
        }
        
        function pagos($x = '',$y = ''){
            $credito = $this->db->get_where('creditos',array('user'=>$_SESSION['user'],'licencia'=>$_SESSION['licencia'],'status'=>1));
            if($credito->num_rows>0){
                $crud = parent::crud_function($x, $y);
                $crud->where('user',$_SESSION['user']);
                $crud->set_relation('banco','bancos_enlaceglobal','{nombre} {nro_cuenta}');            
                $crud->set_field_upload('soporte','files');
                $crud->unset_fields('status')
                     ->unset_columns('user','credito');
                
                $crud->field_type('user','hidden',$_SESSION['user']);
                $crud->field_type('credito','hidden',$credito->row()->id);
                $crud->display_as('monto_recibido','Monto depositado');
                $crud->callback_field('monto_recibido',function($val,$row){
                    $pagos = 0;
                    $credito = $this->db->get_where('creditos',array('user'=>$_SESSION['user'],'licencia'=>$_SESSION['licencia'],'status'=>1))->row();
                    foreach($this->db->get_where('pagos',array('credito'=>$credito->id,'status'=>1))->result() as $p)
                        $pagos+=$p->monto_recibido;
                    return form_input('monto_recibido',$credito->total_pagar-$pagos,'id="field-monto_recibido" class="form-control" placeholder="Cuanto has depositado del credito"');
                });
                if(empty($x) || $x=='ajax_list' || $x=='success')
                    $crud->field_type('status','dropdown',array('-1'=>'Rechazado','0'=>'Validando el pago','1'=>'Validado'));
                $output = $crud->render();                            
                $this->loadView($output);
            }
            else $this->loadView(array('view'=>'panel','crud'=>'user','output'=>$this->error('no existen creditos activos')));
        }
        
        function comisiones($x = '',$y = ''){
            $crud = parent::crud_function($x, $y);
            $crud->set_relation('user','user','{nombre}');
            foreach($this->db->get_where('nodos',array('asociado'=>$_SESSION['user'],'asociado_licencia'=>$_SESSION['licencia']))->result() as $x){
                $crud->or_where('creditos.user = '.$x->user.' AND creditos.licencia = '.$x->licencia,'',FALSE);                
            }
            $crud->field_type('status','dropdown',array('0'=>'Esperando liberación','1'=>'Credito liberado y activo','2'=>'Credito Pagado'));
            $crud->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->columns('user','total_pagar','total_pagado','status','comision');          
            $crud->callback_column('comision',function($val,$row){
                $val = $row->total_pagar*0.04;
                return (string)$val;
            });
            $output = $crud->render();                 
            $this->loadView($output);  
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */